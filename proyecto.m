data = load('familia.txt');
x = data(:,2);
y = data(:,1);
function plotData(x,y)
plot(x,y,'rx','MarkerSize',8); 
end
plotData(x,y);
xlabel('Educacion'); 
ylabel('Ingresos'); 
fprintf('Program paused. Press enter to continue.\n');
pause;

m = length(x);
X = [ones(m, 1) x];

theta = (pinv(X'*X))*X'*y

hold on; 
plot(X(:,2), X*theta, '-')
legend('Training data', 'Linear regression')
hold off